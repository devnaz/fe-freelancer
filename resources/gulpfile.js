var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();


gulp.task('sass', function () {
    gulp.src('sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefixer({
        browsers: ['last 5 versions', '> 5%', 'ie 8']
    }))
    .pipe(gulp.dest('../assets/css'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: '.././'
        }
    })

    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('.././**/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'server']);
